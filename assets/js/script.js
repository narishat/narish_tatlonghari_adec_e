$(function(){
  $('#ph-map').vectorMap({
    map: 'ph_mill_en',
    backgroundColor: '#364959',
    series: {
      regions: [{
          values: {
              'PH-NUE': '#F8AE47',
              'PH-MDC': '#EE5B2F',
              'PH-ILN': '#F8AE47',
              'PH-CAG': '#EE5B2F',
              'PH-ZMB': '#F8AE47',
              'PH-BTN': '#EE5B2F',
              'PH-NUV': '#F8AE47',
              'PH-BEN': '#EE5B2F',
              'PH-LAG': '#F8AE47',
              'PH-LUN': '#EE5B2F',
              'PH-APA': '#F8AE47',
              'PH-QUI': '#EE5B2F',
              'PH-IFU': '#F8AE47',
              'PH-BTG': '#EE5B2F',
              'PH-CAV': '#F8AE47',
              'PH-ISA': '#EE5B2F',
              'PH-RIZ': '#F8AE47',
              'PH-MOU': '#EE5B2F',
              'PH-QUE': '#F8AE47',
              'PH-ILS': '#EE5B2F',
              'PH-PAN': '#F8AE47',
              'PH-TAR': '#EE5B2F',
              'PH-BUL': '#F8AE47',
              'PH-BAN': '#EE5B2F',
              'PH-PAM': '#F8AE47',
              'PH-AUR': '#EE5B2F',
              'PH-ABR': '#F8AE47',
              'PH-KAL': '#EE5B2F',
              'PH-CAN': '#F8AE47',
              'PH-MDR': '#F8AE47',


              'PH-SIG': '#1072B8',
              'PH-EAS': '#2CB475',
              'PH-CAM': '#1072B8',
              'PH-NER': '#2CB475',
              'PH-CEB': '#1072B8',
              'PH-BOH': '#2CB475',
              'PH-CAP': '#1072B8',
              'PH-CAS': '#2CB475',
              'PH-CAT': '#1072B8',
              'PH-MAS': '#2CB475',
              'PH-ROM': '#1072B8',
              'PH-NEC': '#2CB475',
              'PH-ILI': '#1072B8',
              'PH-WSA': '#2CB475',
              'PH-MAD': '#1072B8',
              'PH-SLE': '#2CB475',
              'PH-PLW': '#1072B8',
              'PH-NSA': '#2CB475',
              'PH-GUI': '#1072B8',
              'PH-ALB': '#2CB475',
              'PH-AKL': '#1072B8',
              'PH-LEY': '#2CB475',
              'PH-ANT': '#1072B8',
              'PH-SOR': '#2CB475',
              'PH-BIL': '#1072B8',


              'PH-SUK': '#A9409D',
              'PH-SUR': '#912D8D',
              'PH-SLU': '#A9409D',
              'PH-TAW': '#912D8D',
              'PH-DAO': '#A9409D',
              'PH-SAR': '#912D8D',
              'PH-DAV': '#912D8D',
              'PH-DAS': '#A9409D',
              'PH-NCO': '#A9409D',
              'PH-ZSI': '#912D8D',
              'PH-SCO': '#A9409D',
              'PH-LAS': '#912D8D',
              'PH-LAN': '#A9409D',
              'PH-MSR': '#912D8D',
              'PH-AGS': '#A9409D',
              'PH-MAG': '#912D8D',
              'PH-BUK': '#912D8D',
              'PH-AGN': '#A9409D',
              'PH-SUN': '#A9409D',
              'PH-ZAN': '#912D8D',
              'PH-BAS': '#A9409D',
              'PH-ZAS': '#912D8D',
              'PH-MSC': '#A9409D',
              'PH-COM': '#912D8D',
          },
          attribute: 'fill'
      }]
    },
  });
});
getJsonData("CEB");
function getJsonData(x){
  var selectedRegion = x;
  $(".carousel-inner").empty();
  $("#talentList").empty();
  $("#infraList").empty();
  $("#iframeMap").empty();
  var counter = 1;
  var rowsCounter = 1;
  var loopCount = 1;

  $.each(dataJson, function(r, value) {
    $.each(value, function(a, val) {
      if(selectedRegion === val.abbr){
        $("#regionName").html('<p class="text-left mb-0 p-2 bg-blue text-uppercase text-white">'+val.region+'</p>');
        $("#description").html(val.description);
        $("#information").html(val.info);
        $("#talentTitle").html(`<span class="d-block text-left p-2">TALENTS </span>`);
        $("#infraTitle").html(`<span class="d-block text-left p-2">INFRASTRUCTURE </span>`);
        $("#iframeMap").append(`<iframe src="`+val.map+`" width="100%" frameborder="0" style="border:0; min-height: 232px;" allowfullscreen=""></iframe>`);
          $.each(val.talent, function(c, talents) {
            $("#talentList").append(`<li class="d-block text-left"> <i class="la la-universal-access"></i>`+talents+` </li>`);
          });
          $.each(val.infra, function(c, infra) {
            $("#infraList").append(`<li class="d-block text-left"> <i class="la la-universal-access"></i>`+infra+` </li>`);
          });
          $.each(val.imgsrc, function(c, images) {
            if(counter % 3 === 1){
              if(counter === 1){
                $(".carousel-inner").append(`
                    <div class="carousel-item active">
                      <div class="row rowList-`+counter+`">
                      </div>
                    </div>
                      `);
              }
              else{
                $(".carousel-inner").append(`
                    <div class="carousel-item">
                      <div class="row rowList-`+counter+`">
                      </div>
                    </div>
                      `);
              }
            }
            counter++;
          });
          $.each(val.imgsrc, function(d, images) {
            if(rowsCounter <= 3){
              $(".rowList-"+loopCount).append(`
                <div class="col-4">
                  <div class="card mt-2 mb-4">
                    <div class="view overlay">
                      <img class="card-img-top" src="`+images+`" alt="Card image cap" width="100%" height="120px">
                      <a href="#!">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                    </div>
                    <div class="card-body text-center">
                      <h6 class="card-title">`+d+`</h6>
                    </div>
                  </div>
                </div>
              `);
            }
            else{
              rowsCounter = 0;
              loopCount = loopCount + 3;
            }
            rowsCounter++;
          });
      }
    });
  });
}

$("body").on("click", "#btnFilter" , function(event) {
  //getJsonData();

  $("#countryList").empty();
  var searchPopulation = $("#rangePopulation").val();
  var searchGraduate = $("#rangeGraduates").val();
  var searchCoesCods = $("#rangeCoesCods").val();
  var searchTalents = $("#rangeProgTalents").val();
  var searchEduc = $("#rangeHighEduc").val();
  var searchEmployed = $("#rangeEmployedIT").val();
  $.each(dataJson, function(r, value) {
    $.each(value, function(a, val) {
      if((searchPopulation >= parseInt(val.population)) || (searchGraduate >= parseInt(val.totalGrad)) || (searchCoesCods >= parseInt(val.coecsCods)) || (searchTalents >= parseInt(val.progTalents)) || (searchEduc >= parseInt(val.higherEducation)) || (searchEmployed >= parseInt(val.employed)) ){
        $("#countryList").append("<li class='mx-0 my-1 py-1 text-white text-center purple-gradient selectCountry' data-attr='"+val.abbr+"'>"+val.abbr+"</li>")
      }
    });
  });
  if ($(window).width() < 768) {
    $('html, body').animate({
          scrollTop: $("#content-grid").offset().top
      }, 2000);
  }

});

$("body").on("click", "#countryList .selectCountry" , function(event) {
  var sendRegion = $(this).data('attr');
  getJsonData(sendRegion);
});


$("body").on("click", "#btnReset" , function(event) {
  $("#rangePopulation").val("500025");
  $("#rangeGraduates").val("500025");
  $("#rangeCoesCods").val("500025");
  $("#rangeProgTalents").val("500025");
  $("#rangeHighEduc").val("500025");
  $("#rangeEmployedIT").val("500025");
  getJsonData("CEB");
});
