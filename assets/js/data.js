var dataJson = {
  "data": [
    {
      "region": "cebu",
      "abbr": "CEB",
      "population": "500",
      "totalGrad": "500",
      "coecsCods": "500",
      "progTalents": "500",
      "higherEducation": "500",
      "employed": "500",
      "imgsrc": {
        "Maolbaol":"./assets/img/gallery/maol.png",
        "Badian":"./assets/img/gallery/badian.png",
        "Algeria":"./assets/img/gallery/algeria.png",
        "NCeb":"./assets/img/gallery/maol.png",
        "SCeb":"./assets/img/gallery/badian.png",
        "ECeb":"./assets/img/gallery/algeria.png",
        "WCeb":"./assets/img/gallery/maol.png",
        "NWCeb":"./assets/img/gallery/maol.png",
        "OneMore":"./assets/img/gallery/maol.png"

      },
      "description": "The name &rdquo;Cebu&rdquo; came from the old Cebuano word sibu or sibo (&rdquo;trade&rdquo;), a shortened form of sinibuayng hingpit (&rdquo;the place for trading&rdquo;). It was originally applied to the harbors of the town of Sugbu, the ancient name for Cebu City. Alternate renditions of the name by traders between the 13th to 16th centuries include Sebu, Sibuy, Zubu, or Zebu, among others.Sugbu, in turn, was derived from the Old Cebuano term for &rdquo;scorched earth&rdquo; or &rdquo;great fire&rdquo;.",
      "info": "The city is a major hub for the business process outsourcing industry of the Philippines. In 2013, Cebu ranked 8th worldwide in the &rdquoTop 100 BPO Destinations Report&rdquo by global advisory firm, Tholons. In 2012, the growth in IT-BPO revenues in Cebu grew 26.9 percent at $484 million, while nationally, the industry grew 18.2 percent at $13 billion. <br> Aboitiz Equity Ventures, formerly known as Cebu Pan Asian Holdings, is the first holding company from Cebu City publicly listed in the Philippine Stock Exchange. Ayala Corporation, through its subsidiary Cebu Holdings, Inc. and Cebu Property, both publicly in the PSE Index, developed the Cebu Park District where the mixed-used development zones of the Cebu Business Park and Cebu IT Park are located. Both master planned areas are host to regional headquarters for various companies in the banking, finance, IT and tourism sectors among others.",
      "infra": [
        "5000",
        "5000",
        "5000",
        "5000",
        "5000"
      ],
      "talent": [
        "500",
        "YES",
        "5000",
        "50",
        "5",
        "50000"
      ],
      "map": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d125585.22372709139!2d123.77625413271971!3d10.378756863745503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33a999258dcd2dfd%3A0x4c34030cdbd33507!2sCebu%20City%2C%20Cebu!5e0!3m2!1sen!2sph!4v1575035777886!5m2!1sen!2sph"
    },
    {
      "region": "cavite",
      "abbr": "CAV",
      "population": "400000",
      "totalGrad": "400000",
      "coecsCods": "400000",
      "progTalents": "400000",
      "higherEducation": "400000",
      "employed": "400000",
      "imgsrc": {
        "Balite Falls":"./assets/img/gallery/balite-falls.png",
        "Aguinaldo Shrine":"./assets/img/gallery/kawit.jpg",
        "Picnic Groove":"./assets/img/gallery/picnicgroove.jpg",
        "Test 1":"./assets/img/gallery/picnicgroove.jpg",
        "Test 2":"./assets/img/gallery/kawit.jpg"
      },
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      "info" : "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
      "infra": [
        "5000",
        "5000",
        "5000",
        "5000",
        "5000"
      ],
      "talent": [
        "5000",
        "YES",
        "5000",
        "5000",
        "5000",
        "5000"
      ],
      "map": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d247457.86572096302!2d120.67930149549994!3d14.280659512055415!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397d4eae8163c71%3A0xf0c4d0843bdde727!2sCavite!5e0!3m2!1sen!2sph!4v1575036145661!5m2!1sen!2sph"
    }
  ]
}
